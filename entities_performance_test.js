/**
 * list of Class IDs seen in Danger Zone
 * @type {number[]}
 */
const classIds = [125, 49, 50, 27, 105, 127, 124, 104, 28, 29, 126, 111, 171]
/**
 * how fast it ran with GetEntitiesByClassId in miliseconds
 * @type {number[]}
 */
const speedClassId = []
/**
 * how fast it ran with GetEntities in miliseconds
 * @type {number[]}
 */
const speedEntities = []


// on draw used to test performance
function onDraw()
{
    const performance1 = performance.now()

    // Test GetEntitiesByClassID
    const result = []
    for(var i=0; i<classIds.length; ++i)
    {
        const id = classIds[i]
        const entities = Entity.GetEntitiesByClassID(id)
        // foreach entity with id, add to result
        for(var j=0; j<entities.length; ++j)
            result.push(entities[j])
    }

    const performance2 = performance.now()

    // Test GetEntities
    const result2 = []
    const entities = Entity.GetEntities()
    for(var i=0; i<entities.length; ++i)
    {
        const id = Entity.GetClassID(entities[i])
        if(classIds.indexOf(id) > -1)
            result2.push(entities[i])
    }

    const performance3 = performance.now()
    // log performance
    speedClassId.push( performance3 - performance2 )
    speedEntities.push( performance2 - performance1 )
}
Global.RegisterCallback("Draw", "onDraw")


// Disconnect used to finish performance and show results
function onDisconnect()
{
    Global.Print("\n\n\n\n\nTESTS FINISHED:")

    // calculate average for GetEntitiesByClassID
    var sumClassId = 0
    for(var i=0; i < speedClassId.length; ++i)
        sumClassId += speedClassId[i]
    var averageClassId = sumClassId / speedClassId.length

    // calculate average for GetEntities
    var SumEntities = 0
    for(var i=0; i < speedEntities.length; ++i)
        SumEntities += speedEntities[i]
    var averageEntities = SumEntities / speedEntities.length

    // show results
    Global.Print("Tested " + speedClassId.length + " times.\n")
    Global.Print("Average with GetEntitiesByClassID: " + averageClassId + "\n")
    Global.Print("Average with GetEntities: " + averageEntities + "\n")
    Global.Print("\n\n\n\n")
}
Global.RegisterCallback("cs_game_disconnected", "onDisconnect")
